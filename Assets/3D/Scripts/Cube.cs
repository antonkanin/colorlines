﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Cube : MonoBehaviour
{
    [SerializeField] private EColor m_color;
    public static Action AddBalls;

    public EColor Color
    {
        get { return m_color; }
        set { m_color = value; }
    }

    private void OnMouseDown()
    {
        if (m_color == EColor.Red)//None
        {
            var balls = FindObjectsOfType<Animator>();
            foreach (var ball in balls)
            {
                if (ball.GetBool("Jump"))
                {
                    ball.transform.position = gameObject.transform.localPosition;
                    ball.GetComponent<Animator>().SetBool("Jump", false);
                    AddBalls();
                    break;
                }
            }
        }
    }


}
