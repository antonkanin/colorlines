﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateBall : MonoBehaviour
{
    [SerializeField] private int m_colBalls = 3;
    [SerializeField] private GameObject m_ball;
    [SerializeField] private PlayingField m_field;

    private void Start()
    {
        Cube.AddBalls += CreateBalls;
        CreateBalls();
    }

    private void OnDestroy()
    {
        Cube.AddBalls -= CreateBalls;
    }

    private void CreateBalls()
    {
        var array = m_field.FieldArray;
        int curBalls = 0;
        List<GameObject> cubes = new List<GameObject>();
        while (curBalls < m_colBalls)
        {
            for (int z = 0; z < array.GetLength(0); z++)
            {
                for (int x = 0; x < array.GetLength(0); x++)
                {
                    if (array[z, x].GetComponent<Cube>().Color == EColor.Red)//None
                    {
                        cubes.Add(array[z, x]);
                    }
                }
            }

            if (cubes.Count == 0)
            {
                //TODO end of games
                return;
            }

            var numCubes = RandomValue(0, cubes.Count);
            var positionBall = cubes[numCubes].transform.localPosition;
            var ball = Instantiate(m_ball, positionBall, transform.rotation);
            var color = (EColor)RandomValue(1, 8);
            ball.GetComponent<Ball3D>().Color = color;
            cubes[numCubes].GetComponent<Cube>().Color = color;

            curBalls++;
            cubes.Clear();
        }
        m_field.FieldArray = array;
    }


    private int RandomValue(int min, int max)
    {
        return Random.Range(min, max);
    }
}
