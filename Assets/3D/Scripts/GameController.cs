﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    private GameObject m_ballActivate;

    public GameObject BallActivate
    {
        get { return m_ballActivate; }
        set
        {
            m_ballActivate = value;
            Debug.Log(m_ballActivate);
        }
    }
}
