﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class Ball3D : MonoBehaviour
{
    [SerializeField] private EColor m_color;
    [SerializeField] private Material[] m_materialColor;
    private Animator m_animator;
    private GameController m_gameController;

    private void Start()
    {
        m_animator = GetComponent<Animator>();
    }

    public EColor Color
    {
        get { return m_color; }
        set
        {
            m_color = value;
            BallChangeColor();
        }
    }

#if UNITY_EDITOR
    private void Update()
    {
        BallChangeColor();
    }
#endif

    private void BallChangeColor()
    {
        if (Color != EColor.Red) //None
        {
            gameObject.GetComponent<MeshRenderer>().material = m_materialColor[(int)Color - 1];
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void OnMouseDown()
    {
        if (m_animator.GetBool("Jump"))
        {
            m_animator.SetBool("Jump", false);
        }
        else
        {
            var animators = FindObjectsOfType<Animator>();
            foreach (var animator in animators)
            {
                if (animator.GetBool("Jump"))
                {
                    animator.SetBool("Jump", false);
                    break;
                }
            }
            m_animator.SetBool("Jump", true);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        CubeChangeColor(other, m_color);
    }

    private void OnTriggerExit(Collider other)
    {
        CubeChangeColor(other, EColor.Red); //None
    }

    private void CubeChangeColor(Collider other, EColor color)
    {
        var cube = other.GetComponent<Cube>();
        if (cube != null)
        {
            cube.Color = color;
        }
    }

}
