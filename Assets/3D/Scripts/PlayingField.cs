﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class PlayingField : MonoBehaviour
{
    [SerializeField] private GameObject m_cube;

    private GameObject[,] m_fieldArray;
    private int m_length = 9;

    public GameObject[,] FieldArray
    {
        get { return m_fieldArray; }
        set { m_fieldArray = value; }
    }

    private void Awake()
    {
        if (transform.childCount != 0)
        {
            FieldRemove();
        }
        FieldCreation();
    }

    [ContextMenu("Field Creation")]
    public void FieldCreation()
    {
        var posY = gameObject.transform.position.y;
        FieldArray = new GameObject[m_length, m_length];
        for (int z = 0; z < m_length; z++)
        {
            for (int x = 0; x < m_length; x++)
            {
                FieldArray[z, x] = Instantiate(m_cube, new Vector3(x, posY, z), Quaternion.identity, transform);
            }
        }
    }

    [ContextMenu("Field Remove")]
    public void FieldRemove()
    {
        int i = 0;
        GameObject[] allCube = new GameObject[transform.childCount];

        foreach (Transform child in transform)
        {
            allCube[i] = child.gameObject;
            i++;
        }

        foreach (GameObject cube in allCube)
        {
            DestroyImmediate(cube.gameObject);
        }
    }
}
