﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallsRemove : MonoBehaviour
{
    private List<GameObject> m_identicalBallsHorizontal = new List<GameObject>();
    private List<GameObject> m_identicalBallsVertical = new List<GameObject>();
    private List<GameObject> m_identicalBallsDiagonalTop = new List<GameObject>();
    private List<GameObject> m_identicalBallsDiagonalBottom = new List<GameObject>();
    private int m_firstPosX;
    private int m_firstPosY;
    private EColor m_firstColor;
    private ScoreManager m_scoreManager;

    private static bool m_isLineCheck = false;
    public static bool IsLineCheck
    {
        get { return m_isLineCheck; }
        set { m_isLineCheck = false; }
    }

    private void Start()
    {
        m_scoreManager = FindObjectOfType<ScoreManager>();
        SwipeBall.LineCheck.AddListener(LineCheck);
    }

    private void OnDisable()
    {
        SwipeBall.LineCheck.RemoveListener(LineCheck);
    }

    private void LineCheck(GameObject firstBall)
    {
        m_firstPosX = (int)firstBall.transform.position.x;
        m_firstPosY = (int)firstBall.transform.position.y;
        m_firstColor = firstBall.GetComponent<Ball>().Color;

        LineCheckHorizontal();
        LineCheckVertical();
        LineCheckDiagonalTop();
        LineCheckDiagonalBottom();

        RemoveLine(m_identicalBallsHorizontal);
        RemoveLine(m_identicalBallsVertical);
        RemoveLine(m_identicalBallsDiagonalTop);
        RemoveLine(m_identicalBallsDiagonalBottom);

        if (m_isLineCheck)
        {
            firstBall.GetComponent<Ball>().AnimDestroy = true;
            m_scoreManager.Score = 1;
            //ScoreManager.Score = 1;
        }
    }


    private void RemoveLine(List<GameObject> line)
    {
        if (line.Count >= 4)
        {
            m_isLineCheck = true;
            m_scoreManager.Score = line.Count;
            //ScoreManager.Score = line.Count;
            foreach (var ball in line)
            {
                ball.GetComponent<Ball>().AnimDestroy = true;
            }
        }
        line.Clear();
    }

    // Anton: явный копипаст, нужно общий код в отдельный метод
    private void LineCheckHorizontal()
    {
        for (int x = m_firstPosX - 1; x >= 0; x--) //смотрим влево
        {
            if (BallsArray.ballsArray[x, m_firstPosY] != null)
            {
                var curColor = BallsArray.ballsArray[x, m_firstPosY].GetComponent<Ball>().Color;
                if (curColor == m_firstColor)
                {
                    m_identicalBallsHorizontal.Add(BallsArray.ballsArray[x, m_firstPosY]);
                }
                else
                {
                    break;
                }
            }
            else
            {
                break;
            }
        }

        for (int x = m_firstPosX + 1; x < 9; x++) //смотрим вправо
        {
            if (BallsArray.ballsArray[x, m_firstPosY] != null)
            {
                var curColor = BallsArray.ballsArray[x, m_firstPosY].GetComponent<Ball>().Color;
                if (curColor == m_firstColor)
                {
                    m_identicalBallsHorizontal.Add(BallsArray.ballsArray[x, m_firstPosY]);
                }
                else
                {
                    break;
                }
            }
            else
            {
                break;
            }
        }
    }


    // Anton: явный копипаст, нужно вынести общий код в отдельный метод
    private void LineCheckVertical()
    {
        for (int y = m_firstPosY - 1; y >= 0; y--) //смотрим вниз
        {
            if (BallsArray.ballsArray[m_firstPosX, y] != null)
            {
                // Anton: можно GetComponent<Ball> закэшировать в пуле шаров
                var curColor = BallsArray.ballsArray[m_firstPosX, y].GetComponent<Ball>().Color;
                if (curColor == m_firstColor)
                {
                    m_identicalBallsVertical.Add(BallsArray.ballsArray[m_firstPosX, y]);
                }
                else
                {
                    break;
                }
            }
            else
            {
                break;
            }
        }

        for (int y = m_firstPosY + 1; y < 9; y++) //смотрим вверх
        {
            if (BallsArray.ballsArray[m_firstPosX, y] != null)
            {
                var curColor = BallsArray.ballsArray[m_firstPosX, y].GetComponent<Ball>().Color;
                if (curColor == m_firstColor)
                {
                    m_identicalBallsVertical.Add(BallsArray.ballsArray[m_firstPosX, y]);
                }
                else
                {
                    break;
                }
            }
            else
            {
                break;
            }
        }
    }

    private void LineCheckDiagonalTop()
    {
        int x = m_firstPosX - 1;
        int y = m_firstPosY - 1;
        while (x >= 0 && y >= 0)
        {
            if (BallsArray.ballsArray[x, y] != null)
            {
                var curColor = BallsArray.ballsArray[x, y].GetComponent<Ball>().Color;
                if (curColor == m_firstColor)
                {
                    m_identicalBallsDiagonalTop.Add(BallsArray.ballsArray[x, y]);
                    x--;
                    y--;
                }
                else
                {
                    break;
                }
            }
            else
            {
                break;
            }
        }

        x = m_firstPosX + 1;
        y = m_firstPosY + 1;
        while (x < 9 && y < 9)
        {
            if (BallsArray.ballsArray[x, y] != null)
            {
                var curColor = BallsArray.ballsArray[x, y].GetComponent<Ball>().Color;
                if (curColor == m_firstColor)
                {
                    m_identicalBallsDiagonalTop.Add(BallsArray.ballsArray[x, y]);
                    x++;
                    y++;
                }
                else
                {
                    break;
                }
            }
            else
            {
                break;
            }
        }
    }
    
    private void LineCheckDiagonalBottom()
    {
        int x = m_firstPosX + 1;
        int y = m_firstPosY - 1;
        while (x < 9 && y >= 0)
        {
            if (BallsArray.ballsArray[x, y] != null)
            {
                var curColor = BallsArray.ballsArray[x, y].GetComponent<Ball>().Color;
                if (curColor == m_firstColor)
                {
                    m_identicalBallsDiagonalBottom.Add(BallsArray.ballsArray[x, y]);
                    x++;
                    y--;
                }
                else
                {
                    break;
                }
            }
            else
            {
                break;
            }
        }

        x = m_firstPosX - 1;
        y = m_firstPosY + 1;
        while (x >= 0 && y < 9)
        {
            if (BallsArray.ballsArray[x, y] != null)
            {
                var curColor = BallsArray.ballsArray[x, y].GetComponent<Ball>().Color;
                if (curColor == m_firstColor)
                {
                    m_identicalBallsDiagonalBottom.Add(BallsArray.ballsArray[x, y]);
                    x--;
                    y++;
                }
                else
                {
                    break;
                }
            }
            else
            {
                break;
            }
        }
    }
}
