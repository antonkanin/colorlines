﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallsAdd : MonoBehaviour
{
    [SerializeField] private GameObject m_ball;
    private int m_numBalls = 3;

    private void Start()
    {
        AddBalls();
        SwipeBall.AddBalls.AddListener(AddBalls);
    }

    private void OnDisable()
    {
        SwipeBall.AddBalls.RemoveListener(AddBalls);
    }

    private void AddBalls()
    {
        int curNumBalls = 0;
        while (curNumBalls < m_numBalls)
        {
            int numNullObj = 0;
            foreach (var ball in BallsArray.ballsArray)
            {
                if (ball == null)
                {
                    numNullObj++;
                }
            }

            if (numNullObj <= 0)
            {
                Debug.Log("End Game");
                return; //TODO end game
            }

            int x = Random.Range(0, 9);
            int y = Random.Range(0, 9);
            if (BallsArray.ballsArray[x, y] == null)
            {
                var ballPosition = new Vector3(x, y, 0);
                var ball = Instantiate(m_ball, ballPosition, transform.rotation);
                BallsArray.ballsArray[x, y] = ball;
                curNumBalls++;
            }
        }
    }
}
