﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[ExecuteInEditMode]
public class Field : MonoBehaviour
{
    [SerializeField] private GameObject m_cell;
    private int m_length = 9;

    private void Awake()
    {
        if (transform.childCount != 0)
        {
            FieldRemove();
        }
        FieldCreation();
    }

    [ContextMenu("Field Creation")]
    public void FieldCreation()
    {
        for (int y = 0; y < m_length; y++)
        {
            for (int x = 0; x < m_length; x++)
            {
                Instantiate(m_cell, new Vector3(x, y, 2), Quaternion.identity, transform);
            }
        }
    }

    [ContextMenu("Field Remove")]
    public void FieldRemove()
    {
        int i = 0;
        GameObject[] cells = new GameObject[transform.childCount];

        foreach (Transform child in transform)
        {
            cells[i] = child.gameObject;
            i++;
        }

        foreach (GameObject cell in cells)
        {
            DestroyImmediate(cell.gameObject);
        }
    }
}
