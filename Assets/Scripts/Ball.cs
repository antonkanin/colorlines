﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    [SerializeField] private EColor m_color;
    [SerializeField] private Sprite[] m_sprites;
    private Animator m_animator;

    private void Start()
    {
        m_animator = GetComponent<Animator>();
        AssignColor();
    }

    // Anton: я бы AnimDestroy сделал просто как метод, не как property?
    // Property используется когда хранится какое-то значение
    // public void AnimDestroy()
    // {
    // ...
    // }
    public bool AnimDestroy
    {
        set
        {
            m_animator.SetTrigger("Destroy");
            Destroy(gameObject, 0.5f);
        }
    }

    public EColor Color
    {
        get { return m_color; }
        set { m_color = value; }
    }

    private void AssignColor()
    {
        int colorNumber = Random.Range(0, (int)EColor.Length);
        m_color = (EColor)colorNumber;
        gameObject.GetComponent<SpriteRenderer>().sprite = m_sprites[colorNumber];
    }

    // Anton: убрать пустой метод
    private void OnDestroy()
    {
        
    }
}
