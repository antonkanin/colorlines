﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Events;

public class LineCheckEvent : UnityEvent<GameObject> { }

public class SwipeBall : MonoBehaviour
{
    private bool m_mouseDown = false;

    // Anton: переименовать m_firstPosition в m_initialBallPosition
    private Vector3 m_firstPosition;

    public static UnityEvent AddBalls = new UnityEvent();
    public static LineCheckEvent LineCheck = new LineCheckEvent();

    private void OnMouseDown()
    {
        if (Time.timeScale != 0)
        {
            m_firstPosition = transform.position;
            m_mouseDown = true;
        }
    }

    // я бы разделил на методы поменьше, как-то многовато текста
    private void OnMouseUp()
    {
        if (m_mouseDown)
        {
            m_mouseDown = false;
            var ballPosition = transform.position;
            ballPosition.x = Mathf.Round(ballPosition.x);
            ballPosition.y = Mathf.Round(ballPosition.y);
            ballPosition.z = 0;

            var x = (int)ballPosition.x;
            var y = (int)ballPosition.y;

            // Anton: лучше размер доски 8 вынести в константную переменную
            // например в отдельный класс констант
            if (x < 0 || x > 8 || y < 0 || y > 8)
            {
                transform.position = m_firstPosition;
                return;
            }

            if (BallsArray.ballsArray[x, y] == null)
            {
                var firstX = (int)m_firstPosition.x;
                var firstY = (int)m_firstPosition.y;

                BallsArray.ballsArray[x, y] = this.gameObject;
                BallsArray.ballsArray[firstX, firstY] = null;
                transform.position = ballPosition;

                LineCheck.Invoke(this.gameObject);
                if (BallsRemove.IsLineCheck)
                {
                    BallsRemove.IsLineCheck = false;
                }
                else
                {
                    AddBalls.Invoke();
                }
                return;
            }
            transform.position = m_firstPosition;
        }
    }

    private void Update()
    {
        if (m_mouseDown)
        {
            var cursorPosition = Input.mousePosition;
            cursorPosition = Camera.main.ScreenToWorldPoint(cursorPosition);
            cursorPosition.z = -2;
            transform.position = cursorPosition;
        }
    }
}
