﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using TMPro;

public class ScoreManager : MonoBehaviour
{
    private TextMeshProUGUI m_scoreTxt;
    private int m_score;

    private void Start()
    {
        m_scoreTxt = GetComponent<TextMeshProUGUI>();
        m_scoreTxt.text = "score: 0";
    }

    public int Score
    {
        get { return m_score; }
        set
        {
            m_score = m_score + value;
            m_scoreTxt.text = "score: " + m_score;
        }
    }

    public void RecordScore()
    {
        if (PlayerPrefs.GetInt("score") < m_score)
        {
            PlayerPrefs.SetInt("score", m_score);
        }
    }
}
