﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class EndGame : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI m_yourScoreTxt;
    private ScoreManager m_scoreManager;

    private void Start()
    {
        m_scoreManager = FindObjectOfType<ScoreManager>();
        m_scoreManager.RecordScore();
        m_yourScoreTxt.text = "your score:" + m_scoreManager.Score;
    }

    public void Restart()
    {
        SceneManager.LoadScene(1);
    }
}
