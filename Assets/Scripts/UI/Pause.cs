﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pause : MonoBehaviour
{
    private int m_playGame = 1;

    public int PlayGame
    {
        set
        {
            m_playGame = Mathf.Clamp(value, 0, 1);
            Time.timeScale = m_playGame;
        }
    }
}
