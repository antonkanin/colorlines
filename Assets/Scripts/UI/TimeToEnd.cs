﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.Events;

public class TimeToEnd : MonoBehaviour
{
    [SerializeField] private float m_time = 60;

    public UnityEvent EndGame = new UnityEvent();

    [Header("Reference:")]
    [SerializeField] private TextMeshProUGUI m_timeToEndTxt;

    private void Update()
    {
        m_time = Mathf.Clamp((m_time - Time.deltaTime), 0 , float.MaxValue);
        m_timeToEndTxt.text = "" + Mathf.Round(m_time);

        if (m_time <= 0)
        {
            EndGame.Invoke();
        }
    }
}
